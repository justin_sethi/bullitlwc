Webruntime.moduleRegistry.define('c/lookupTestContainer', ['lwc', 'lightning/configProvider', 'force/lds', 'wire-service'], function (lwc, configProvider, lds, wireService) { 'use strict';

   function stylesheet(hostSelector, shadowSelector, nativeShadow) {
     return ".THIS" + shadowSelector + "{width:100%;height: 40px;}\n.THIS" + shadowSelector + " .fullSize" + shadowSelector + "{width:100%;height: 30px;}\n.THIS" + shadowSelector + " .slds-listbox__item:hover" + shadowSelector + " {background: #d8edff;text-shadow: none;color: #16325c;}\n.THIS" + shadowSelector + " .slds-pill" + shadowSelector + " {padding: 1px 0 !important;}\n.THIS" + shadowSelector + " .optionParent1" + shadowSelector + "{line-height: 1.5;padding: 0.25rem 0.75rem;font-size: 0.8125rem;}\n.THIS" + shadowSelector + " .optionIcon" + shadowSelector + " {margin-top: 0.50rem;}\n.THIS" + shadowSelector + " .optionTitle" + shadowSelector + " {max-width: 100%;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;display: block;margin-bottom: 0.125rem;}\n.THIS" + shadowSelector + " .optionline" + shadowSelector + " {display: block;margin-top: -0.25rem;color: #54698d;}\n.THIS" + shadowSelector + " .singleRow" + shadowSelector + " {padding-top:5px;}\n.THIS" + shadowSelector + " .hide" + shadowSelector + " {display:none;}\n";
   }
   var _implicitStylesheets = [stylesheet];

   function stylesheet$1(hostSelector, shadowSelector, nativeShadow) {
     return "_:-ms-lang(x)" + shadowSelector + ", svg" + shadowSelector + " {pointer-events: none;}\n";
   }
   var _implicitStylesheets$1 = [stylesheet$1];

   function tmpl($api, $cmp, $slotset, $ctx) {
     const {
       fid: api_scoped_frag_id,
       h: api_element
     } = $api;
     return [api_element("svg", {
       className: $cmp.computedClass,
       attrs: {
         "focusable": "false",
         "data-key": $cmp.name,
         "aria-hidden": "true"
       },
       key: 2
     }, [api_element("use", {
       attrs: {
         "xlink:href": lwc.sanitizeAttribute("use", "http://www.w3.org/2000/svg", "xlink:href", api_scoped_frag_id($cmp.href))
       },
       key: 3
     }, [])])];
   }

   var _tmpl = lwc.registerTemplate(tmpl);
   tmpl.stylesheets = [];

   if (_implicitStylesheets$1) {
     tmpl.stylesheets.push.apply(tmpl.stylesheets, _implicitStylesheets$1);
   }
   tmpl.stylesheetTokens = {
     hostAttribute: "lightning-primitiveIcon_primitiveIcon-host",
     shadowAttribute: "lightning-primitiveIcon_primitiveIcon"
   };

   const proto = {
     add(className) {
       if (typeof className === 'string') {
         this[className] = true;
       } else {
         Object.assign(this, className);
       }

       return this;
     },

     invert() {
       Object.keys(this).forEach(key => {
         this[key] = !this[key];
       });
       return this;
     },

     toString() {
       return Object.keys(this).filter(key => this[key]).join(' ');
     }

   };
   function classSet(config) {
     if (typeof config === 'string') {
       const key = config;
       config = {};
       config[key] = true;
     }

     return Object.assign(Object.create(proto), config);
   }

   // NOTE: lightning-utils is a public library. adding new utils here means we

   /**
   An emitter implementation based on the Node.js EventEmitter API:
   https://nodejs.org/dist/latest-v6.x/docs/api/events.html#events_class_eventemitter
   **/

   /**
    * Create a deep copy of an object or array
    * @param {object|array} o - item to be copied
    * @returns {object|array} copy of the item
    */

   /**
    * Utility function to generate an unique guid.
    * used on state objects to provide a performance aid when iterating
    * through the items and marking them for render
    * @returns {String} an unique string ID
    */

   function classListMutation(classList, config) {
     Object.keys(config).forEach(key => {
       if (typeof key === 'string' && key.length) {
         if (config[key]) {
           classList.add(key);
         } else {
           classList.remove(key);
         }
       }
     });
   }

   /**
   A string normalization utility for attributes.
   @param {String} value - The value to normalize.
   @param {Object} config - The optional configuration object.
   @param {String} [config.fallbackValue] - The optional fallback value to use if the given value is not provided or invalid. Defaults to an empty string.
   @param {Array} [config.validValues] - An optional array of valid values. Assumes all input is valid if not provided.
   @return {String} - The normalized value.
   **/
   function normalizeString(value, config = {}) {
     const {
       fallbackValue = '',
       validValues,
       toLowerCase = true
     } = config;
     let normalized = typeof value === 'string' && value.trim() || '';
     normalized = toLowerCase ? normalized.toLowerCase() : normalized;

     if (validValues && validValues.indexOf(normalized) === -1) {
       normalized = fallbackValue;
     }

     return normalized;
   }

   const isIE11 = isIE11Test(navigator);
   const isChrome = isChromeTest(navigator); // The following functions are for tests only

   function isIE11Test(navigator) {
     // https://stackoverflow.com/questions/17447373/how-can-i-target-only-internet-explorer-11-with-javascript
     return /Trident.*rv[ :]*11\./.test(navigator.userAgent);
   }
   function isChromeTest(navigator) {
     // https://stackoverflow.com/questions/4565112/javascript-how-to-find-out-if-the-user-browser-is-chrome
     return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
   }

   /**
    * Set an attribute on an element, if it's a normal element
    * it will use setAttribute, if it's an LWC component
    * it will use the public property
    *
    * @param {HTMLElement} element The element to act on
    * @param {String} attribute the attribute to set
    * @param {Any} value the value to set
    */

   // hide panel on scroll

   var _tmpl$1 = void 0;

   // Taken from https://github.com/jonathantneal/svg4everybody/pull/139
   // Remove this iframe-in-edge check once the following is resolved https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/8323875/
   const isEdgeUA = /\bEdge\/.(\d+)\b/.test(navigator.userAgent);
   const inIframe = window.top !== window.self;
   const isIframeInEdge = isEdgeUA && inIframe;
   var isIframeInEdge$1 = lwc.registerComponent(isIframeInEdge, {
     tmpl: _tmpl$1
   });

   // Taken from https://git.soma.salesforce.com/aura/lightning-global/blob/999dc35f948246181510df6e56f45ad4955032c2/src/main/components/lightning/SVGLibrary/stamper.js#L38-L60
   function fetchSvg(url) {
     return new Promise((resolve, reject) => {
       const xhr = new XMLHttpRequest();
       xhr.open('GET', url);
       xhr.send();

       xhr.onreadystatechange = () => {
         if (xhr.readyState === 4) {
           if (xhr.status === 200) {
             resolve(xhr.responseText);
           } else {
             reject(xhr);
           }
         }
       };
     });
   }

   // Which looks like it was inspired by https://github.com/jonathantneal/svg4everybody/blob/377d27208fcad3671ed466e9511556cb9c8b5bd8/lib/svg4everybody.js#L92-L107
   // Modify at your own risk!

   const newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/;
   const webkitUA = /\bAppleWebKit\/(\d+)\b/;
   const olderEdgeUA = /\bEdge\/12\.(\d+)\b/;
   const isIE = newerIEUA.test(navigator.userAgent) || (navigator.userAgent.match(olderEdgeUA) || [])[1] < 10547 || (navigator.userAgent.match(webkitUA) || [])[1] < 537;
   const supportsSvg = !isIE && !isIframeInEdge$1;
   var supportsSvg$1 = lwc.registerComponent(supportsSvg, {
     tmpl: _tmpl$1
   });

   /**
   This polyfill injects SVG sprites into the document for clients that don't
   fully support SVG. We do this globally at the document level for performance
   reasons. This causes us to lose namespacing of IDs across sprites. For example,
   if both #image from utility sprite and #image from doctype sprite need to be
   rendered on the page, both end up as #image from the doctype sprite (last one
   wins). SLDS cannot change their image IDs due to backwards-compatibility
   reasons so we take care of this issue at runtime by adding namespacing as we
   polyfill SVG elements.

   For example, given "/assets/icons/action-sprite/svg/symbols.svg#approval", we
   replace the "#approval" id with "#${namespace}-approval" and a similar
   operation is done on the corresponding symbol element.
   **/
   const svgTagName = /svg/i;

   const isSvgElement = el => el && svgTagName.test(el.nodeName);

   const requestCache = {};
   const symbolEls = {};
   const svgFragments = {};
   const spritesContainerId = 'slds-svg-sprites';
   let spritesEl;
   function polyfill(el) {
     if (!supportsSvg$1 && isSvgElement(el)) {
       if (!spritesEl) {
         spritesEl = document.createElement('svg');
         spritesEl.xmlns = 'http://www.w3.org/2000/svg';
         spritesEl['xmlns:xlink'] = 'http://www.w3.org/1999/xlink';
         spritesEl.style.display = 'none';
         spritesEl.id = spritesContainerId;
         document.body.insertBefore(spritesEl, document.body.childNodes[0]);
       }

       Array.from(el.getElementsByTagName('use')).forEach(use => {
         // We access the href differently in raptor and in aura, probably
         // due to difference in the way the svg is constructed.
         const src = use.getAttribute('xlink:href') || use.getAttribute('href');

         if (src) {
           // "/assets/icons/action-sprite/svg/symbols.svg#approval" =>
           // ["/assets/icons/action-sprite/svg/symbols.svg", "approval"]
           const parts = src.split('#');
           const url = parts[0];
           const id = parts[1];
           const namespace = url.replace(/[^\w]/g, '-');
           const href = `#${namespace}-${id}`;

           if (url.length) {
             // set the HREF value to no longer be an external reference
             if (use.getAttribute('xlink:href')) {
               use.setAttribute('xlink:href', href);
             } else {
               use.setAttribute('href', href);
             } // only insert SVG content if it hasn't already been retrieved


             if (!requestCache[url]) {
               requestCache[url] = fetchSvg(url);
             }

             requestCache[url].then(svgContent => {
               // create a document fragment from the svgContent returned (is parsed by HTML parser)
               if (!svgFragments[url]) {
                 const svgFragment = document.createRange().createContextualFragment(svgContent);
                 svgFragments[url] = svgFragment;
               }

               if (!symbolEls[href]) {
                 const svgFragment = svgFragments[url];
                 const symbolEl = svgFragment.querySelector(`#${id}`);
                 symbolEls[href] = true;
                 symbolEl.id = `${namespace}-${id}`;
                 spritesEl.appendChild(symbolEl);
               }
             });
           }
         }
       });
     }
   }

   const validNameRe = /^([a-zA-Z]+):([a-zA-Z]\w*)$/;
   const underscoreRe = /_/g;
   let pathPrefix;
   const tokenNameMap = Object.assign(Object.create(null), {
     action: 'lightning.actionSprite',
     custom: 'lightning.customSprite',
     doctype: 'lightning.doctypeSprite',
     standard: 'lightning.standardSprite',
     utility: 'lightning.utilitySprite'
   });
   const tokenNameMapRtl = Object.assign(Object.create(null), {
     action: 'lightning.actionSpriteRtl',
     custom: 'lightning.customSpriteRtl',
     doctype: 'lightning.doctypeSpriteRtl',
     standard: 'lightning.standardSpriteRtl',
     utility: 'lightning.utilitySpriteRtl'
   });
   const defaultTokenValueMap = Object.assign(Object.create(null), {
     'lightning.actionSprite': '/assets/icons/action-sprite/svg/symbols.svg',
     'lightning.actionSpriteRtl': '/assets/icons/action-sprite/svg/symbols.svg',
     'lightning.customSprite': '/assets/icons/custom-sprite/svg/symbols.svg',
     'lightning.customSpriteRtl': '/assets/icons/custom-sprite/svg/symbols.svg',
     'lightning.doctypeSprite': '/assets/icons/doctype-sprite/svg/symbols.svg',
     'lightning.doctypeSpriteRtl': '/assets/icons/doctype-sprite/svg/symbols.svg',
     'lightning.standardSprite': '/assets/icons/standard-sprite/svg/symbols.svg',
     'lightning.standardSpriteRtl': '/assets/icons/standard-sprite/svg/symbols.svg',
     'lightning.utilitySprite': '/assets/icons/utility-sprite/svg/symbols.svg',
     'lightning.utilitySpriteRtl': '/assets/icons/utility-sprite/svg/symbols.svg'
   });

   const getDefaultBaseIconPath = (category, nameMap) => defaultTokenValueMap[nameMap[category]];

   const getBaseIconPath = (category, direction) => {
     const nameMap = direction === 'rtl' ? tokenNameMapRtl : tokenNameMap;
     return configProvider.getToken(nameMap[category]) || getDefaultBaseIconPath(category, nameMap);
   };

   const getMatchAtIndex = index => iconName => {
     const result = validNameRe.exec(iconName);
     return result ? result[index] : '';
   };

   const getCategory = getMatchAtIndex(1);
   const getName = getMatchAtIndex(2);
   const isValidName = iconName => validNameRe.test(iconName);
   const getIconPath = (iconName, direction = 'ltr') => {
     pathPrefix = pathPrefix !== undefined ? pathPrefix : configProvider.getPathPrefix();

     if (isValidName(iconName)) {
       const baseIconPath = getBaseIconPath(getCategory(iconName), direction);

       if (baseIconPath) {
         // This check was introduced the following MS-Edge issue:
         // https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/9655192/
         // If and when this get fixed, we can safely remove this block of code.
         if (isIframeInEdge$1) {
           // protocol => 'https:' or 'http:'
           // host => hostname + port
           const origin = `${window.location.protocol}//${window.location.host}`;
           return `${origin}${pathPrefix}${baseIconPath}#${getName(iconName)}`;
         }

         return `${pathPrefix}${baseIconPath}#${getName(iconName)}`;
       }
     }

     return '';
   };
   const computeSldsClass = iconName => {
     if (isValidName(iconName)) {
       const category = getCategory(iconName);
       const name = getName(iconName).replace(underscoreRe, '-');
       return `slds-icon-${category}-${name}`;
     }

     return '';
   };

   const isSafari = window.safari && window.safari.pushNotification && window.safari.pushNotification.toString() === '[object SafariRemoteNotification]'; // [W-3421985] https://bugs.webkit.org/show_bug.cgi?id=162866
   // https://git.soma.salesforce.com/aura/lightning-global/blob/82e8bfd02846fa7e6b3e7549a64be95b619c4b1f/src/main/components/lightning/primitiveIcon/primitiveIconHelper.js#L53-L56

   function safariA11yPatch(svgElement) {
     if (!svgElement || !isSafari) {
       return;
     } // In case we're dealing with a proxied element.


     svgElement = lwc.unwrap(svgElement);
     const use = svgElement.querySelector('use');

     if (!use) {
       return;
     }

     svgElement.insertBefore(document.createTextNode('\n'), use); // If use.nextSibling is null, the text node is added to the end of
     // the list of children of the SVG element.
     // https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore

     svgElement.insertBefore(document.createTextNode('\n'), use.nextSibling);
   }

   class LightningPrimitiveIcon extends lwc.LightningElement {
     constructor(...args) {
       super(...args);
       this.iconName = void 0;
       this.src = void 0;
       this.svgClass = void 0;
       this.size = 'medium';
       this.variant = void 0;
       this.privateIconSvgTemplates = configProvider.getIconSvgTemplates();
     }

     get inlineSvgProvided() {
       return !!this.privateIconSvgTemplates;
     }

     renderedCallback() {
       if (this.iconName !== this.prevIconName && !this.inlineSvgProvided) {
         this.prevIconName = this.iconName;
         const svgElement = this.template.querySelector('svg');
         polyfill(svgElement);
         safariA11yPatch(svgElement);
       }
     }

     get href() {
       return this.src || getIconPath(this.iconName, configProvider.getLocale && configProvider.getLocale().dir);
     }

     get name() {
       return getName(this.iconName);
     }

     get normalizedSize() {
       return normalizeString(this.size, {
         fallbackValue: 'medium',
         validValues: ['xx-small', 'x-small', 'small', 'medium', 'large']
       });
     }

     get normalizedVariant() {
       // NOTE: Leaving a note here because I just wasted a bunch of time
       // investigating why both 'bare' and 'inverse' are supported in
       // lightning-primitive-icon. lightning-icon also has a deprecated
       // 'bare', but that one is synonymous to 'inverse'. This 'bare' means
       // that no classes should be applied. So this component needs to
       // support both 'bare' and 'inverse' while lightning-icon only needs to
       // support 'inverse'.
       return normalizeString(this.variant, {
         fallbackValue: '',
         validValues: ['bare', 'error', 'inverse', 'warning', 'success']
       });
     }

     get computedClass() {
       const {
         normalizedSize,
         normalizedVariant
       } = this;
       const classes = classSet(this.svgClass);

       if (normalizedVariant !== 'bare') {
         classes.add('slds-icon');
       }

       switch (normalizedVariant) {
         case 'error':
           classes.add('slds-icon-text-error');
           break;

         case 'warning':
           classes.add('slds-icon-text-warning');
           break;

         case 'success':
           classes.add('slds-icon-text-success');
           break;

         case 'inverse':
         case 'bare':
           break;

         default:
           // if custom icon is set, we don't want to set
           // the text-default class
           if (!this.src) {
             classes.add('slds-icon-text-default');
           }

       }

       if (normalizedSize !== 'medium') {
         classes.add(`slds-icon_${normalizedSize}`);
       }

       return classes.toString();
     }

     resolveTemplate() {
       const name = this.iconName;

       if (isValidName(name)) {
         const [spriteName, iconName] = name.split(':');
         const template = this.privateIconSvgTemplates[`${spriteName}_${iconName}`];

         if (template) {
           return template;
         }
       }

       return _tmpl;
     }

     render() {
       if (this.inlineSvgProvided) {
         return this.resolveTemplate();
       }

       return _tmpl;
     }

   }

   lwc.registerDecorators(LightningPrimitiveIcon, {
     publicProps: {
       iconName: {
         config: 0
       },
       src: {
         config: 0
       },
       svgClass: {
         config: 0
       },
       size: {
         config: 0
       },
       variant: {
         config: 0
       }
     }
   });

   var _lightningPrimitiveIcon = lwc.registerComponent(LightningPrimitiveIcon, {
     tmpl: _tmpl
   });

   function tmpl$1($api, $cmp, $slotset, $ctx) {
     const {
       c: api_custom_element,
       d: api_dynamic,
       h: api_element
     } = $api;
     return [api_custom_element("lightning-primitive-icon", _lightningPrimitiveIcon, {
       props: {
         "iconName": $cmp.state.iconName,
         "size": $cmp.size,
         "variant": $cmp.variant,
         "src": $cmp.state.src
       },
       key: 2
     }, []), $cmp.alternativeText ? api_element("span", {
       classMap: {
         "slds-assistive-text": true
       },
       key: 3
     }, [api_dynamic($cmp.alternativeText)]) : null];
   }

   var _tmpl$2 = lwc.registerTemplate(tmpl$1);
   tmpl$1.stylesheets = [];
   tmpl$1.stylesheetTokens = {
     hostAttribute: "lightning-icon_icon-host",
     shadowAttribute: "lightning-icon_icon"
   };

   /**
    * Represents a visual element that provides context and enhances usability.
    */

   class LightningIcon extends lwc.LightningElement {
     constructor(...args) {
       super(...args);
       this.state = {};
       this.alternativeText = void 0;
     }

     /**
      * A uri path to a custom svg sprite, including the name of the resouce,
      * for example: /assets/icons/standard-sprite/svg/test.svg#icon-heart
      * @type {string}
      */
     get src() {
       return this.privateSrc;
     }

     set src(value) {
       this.privateSrc = value; // if value is not present, then we set the state back
       // to the original iconName that was passed
       // this might happen if the user sets a custom icon, then
       // decides to revert back to SLDS by removing the src attribute

       if (!value) {
         this.state.iconName = this.iconName;
         this.classList.remove('slds-icon-standard-default');
       } // if isIE11 and the src is set
       // we'd like to show the 'standard:default' icon instead
       // for performance reasons.


       if (value && isIE11) {
         this.setDefault();
         return;
       }

       this.state.src = value;
     }
     /**
      * The Lightning Design System name of the icon.
      * Names are written in the format 'utility:down' where 'utility' is the category,
      * and 'down' is the specific icon to be displayed.
      * @type {string}
      * @required
      */


     get iconName() {
       return this.privateIconName;
     }

     set iconName(value) {
       this.privateIconName = value; // if src is set, we don't need to validate
       // iconName

       if (this.src) {
         return;
       }

       if (isValidName(value)) {
         const isAction = getCategory(value) === 'action'; // update classlist only if new iconName is different than state.iconName
         // otherwise classListMutation receives class:true and class: false and removes slds class

         if (value !== this.state.iconName) {
           classListMutation(this.classList, {
             'slds-icon_container_circle': isAction,
             [computeSldsClass(value)]: true,
             [computeSldsClass(this.state.iconName)]: false
           });
         }

         this.state.iconName = value;
       } else {
         console.warn(`<lightning-icon> Invalid icon name ${value}`); // eslint-disable-line no-console
         // Invalid icon names should render a blank icon. Remove any
         // classes that might have been previously added.

         classListMutation(this.classList, {
           'slds-icon_container_circle': false,
           [computeSldsClass(this.state.iconName)]: false
         });
         this.state.iconName = undefined;
       }
     }
     /**
      * The size of the icon. Options include xx-small, x-small, small, medium, or large.
      * The default is medium.
      * @type {string}
      * @default medium
      */


     get size() {
       return normalizeString(this.state.size, {
         fallbackValue: 'medium',
         validValues: ['xx-small', 'x-small', 'small', 'medium', 'large']
       });
     }

     set size(value) {
       this.state.size = value;
     }
     /**
      * The variant changes the appearance of a utility icon.
      * Accepted variants include inverse, success, warning, and error.
      * Use the inverse variant to implement a white fill in utility icons on dark backgrounds.
      * @type {string}
      */


     get variant() {
       return normalizeVariant(this.state.variant, this.state.iconName);
     }

     set variant(value) {
       this.state.variant = value;
     }

     connectedCallback() {
       this.classList.add('slds-icon_container');
     }

     setDefault() {
       this.state.src = undefined;
       this.state.iconName = 'standard:default';
       this.classList.add('slds-icon-standard-default');
     }

   }

   lwc.registerDecorators(LightningIcon, {
     publicProps: {
       alternativeText: {
         config: 0
       },
       src: {
         config: 3
       },
       iconName: {
         config: 3
       },
       size: {
         config: 3
       },
       variant: {
         config: 3
       }
     },
     track: {
       state: 1
     }
   });

   var _lightningIcon = lwc.registerComponent(LightningIcon, {
     tmpl: _tmpl$2
   });

   function normalizeVariant(variant, iconName) {
     // Unfortunately, the `bare` variant was implemented to do what the
     // `inverse` variant should have done. Keep this logic for as long as
     // we support the `bare` variant.
     if (variant === 'bare') {
       // TODO: Deprecation warning using strippable assertion
       variant = 'inverse';
     }

     if (getCategory(iconName) === 'utility') {
       return normalizeString(variant, {
         fallbackValue: '',
         validValues: ['error', 'inverse', 'warning', 'success']
       });
     }

     return 'inverse';
   }

   function tmpl$2($api, $cmp, $slotset, $ctx) {
     const {
       gid: api_scoped_id,
       b: api_bind,
       h: api_element,
       c: api_custom_element,
       d: api_dynamic,
       t: api_text,
       k: api_key,
       i: api_iterator
     } = $api;
     const {
       _m0,
       _m1,
       _m2
     } = $ctx;
     return [api_element("div", {
       classMap: {
         "slds": true
       },
       key: 2
     }, [api_element("div", {
       classMap: {
         "slds-form-element": true
       },
       key: 3
     }, [api_element("div", {
       classMap: {
         "slds-form-element__control": true
       },
       key: 4
     }, [api_element("div", {
       classMap: {
         "slds-combobox_container": true,
         "slds-has-inline-listbox": true
       },
       key: 5
     }, [api_element("div", {
       classMap: {
         "slds-combobox": true,
         "slds-dropdown-trigger": true,
         "slds-dropdown-trigger_click": true,
         "slds-is-open": true
       },
       styleMap: {
         "width": "95%"
       },
       attrs: {
         "aria-expanded": "true",
         "aria-haspopup": "listbox",
         "role": "combobox"
       },
       key: 6
     }, [api_element("div", {
       classMap: {
         "slds-combobox__form-element": true,
         "slds-input-has-icon": true,
         "slds-input-has-icon_right": true
       },
       key: 7
     }, [$cmp.selItemIsEmpty ? api_element("div", {
       key: 8
     }, [api_element("input", {
       classMap: {
         "slds-input": true,
         "slds-combobox__input": true
       },
       styleMap: {
         "lineHeight": "2"
       },
       attrs: {
         "type": "text",
         "id": api_scoped_id("combobox-unique-id"),
         "aria-activedescendant": `${api_scoped_id("listbox-option-unique-id-01")}`,
         "aria-autocomplete": "list",
         "aria-controls": `${api_scoped_id("listbox-unique-id")}`,
         "autocomplete": "off",
         "role": "combobox",
         "placeholder": $cmp.placeholder
       },
       key: 9,
       on: {
         "keyup": _m0 || ($ctx._m0 = api_bind($cmp.serverCall))
       }
     }, [])]) : null, !$cmp.selItemIsEmpty ? api_element("div", {
       key: 10
     }, [api_element("span", {
       classMap: {
         "slds-pill": true,
         "slds-pill_link": true,
         "fullSize": true
       },
       key: 11
     }, [api_element("a", {
       classMap: {
         "slds-pill__action": true,
         "slds-p-left_x-small": true
       },
       styleMap: {
         "height": "28px"
       },
       attrs: {
         "href": "javascript:void(0);",
         "title": $cmp.selItem.text
       },
       key: 12
     }, [api_custom_element("lightning-icon", _lightningIcon, {
       props: {
         "iconName": $cmp.lookupIcon,
         "size": "x-small"
       },
       key: 13
     }, []), api_element("span", {
       classMap: {
         "slds-pill__label": true,
         "slds-p-left_x-small": true
       },
       key: 14
     }, [api_dynamic($cmp.selItem.text)])]), api_element("button", {
       classMap: {
         "slds-button": true,
         "slds-button_icon": true,
         "slds-pill__remove": true
       },
       attrs: {
         "title": "Remove"
       },
       key: 15,
       on: {
         "click": _m1 || ($ctx._m1 = api_bind($cmp.clearSelection))
       }
     }, [api_custom_element("lightning-icon", _lightningIcon, {
       props: {
         "iconName": "utility:close",
         "size": "small",
         "alternativeText": "Press delete or backspace to remove"
       },
       key: 16
     }, []), api_element("span", {
       classMap: {
         "slds-assistive-text": true
       },
       key: 17
     }, [api_text("Remove")])])])]) : null, $cmp.resultNotNull ? api_element("div", {
       attrs: {
         "id": api_scoped_id("listbox-unique-id"),
         "role": "listbox"
       },
       key: 18
     }, [api_element("ul", {
       classMap: {
         "slds-listbox": true,
         "slds-listbox_vertical": true,
         "slds-dropdown": true,
         "slds-dropdown_fluid": true
       },
       styleMap: {
         "display": "block",
         "minWidth": "auto",
         "maxWidth": "100%",
         "width": "100%"
       },
       attrs: {
         "role": "presentation"
       },
       key: 19
     }, api_iterator($cmp.server_result, function (item) {
       return api_element("li", {
         classMap: {
           "slds-listbox__item": true
         },
         attrs: {
           "role": "presentation"
         },
         key: api_key(21, item.id),
         on: {
           "click": _m2 || ($ctx._m2 = api_bind($cmp.itemSelected))
         }
       }, [api_element("span", {
         classMap: {
           "slds-media": true,
           "slds-listbox__option": true,
           "slds-listbox__option_entity": true,
           "slds-listbox__option_has-meta": true
         },
         attrs: {
           "role": "option"
         },
         key: 22
       }, [api_element("span", {
         classMap: {
           "slds-media__figure": true,
           "optionIcon": true
         },
         key: 23
       }, [api_element("span", {
         classMap: {
           "slds-icon_container": true
         },
         key: 24
       }, [api_custom_element("lightning-icon", _lightningIcon, {
         props: {
           "iconName": $cmp.lookupIcon,
           "size": "small"
         },
         key: 25
       }, []), api_element("span", {
         classMap: {
           "slds-assistive-text": true
         },
         key: 26
       }, [api_dynamic($cmp.objectName)])])]), api_element("span", {
         classMap: {
           "slds-media__body": true,
           "singleRow": true
         },
         key: 27
       }, [api_element("span", {
         classMap: {
           "optionTitle": true,
           "slds-listbox__option-text": true,
           "slds-listbox__option-text_entity": true
         },
         key: 28
       }, [api_dynamic(item.text)])])])]);
     }))]) : null])])])])])])];
   }

   var _tmpl$3 = lwc.registerTemplate(tmpl$2);
   tmpl$2.stylesheets = [];

   if (_implicitStylesheets) {
     tmpl$2.stylesheets.push.apply(tmpl$2.stylesheets, _implicitStylesheets);
   }
   tmpl$2.stylesheetTokens = {
     hostAttribute: "lwc-lookup_lookup-host",
     shadowAttribute: "lwc-lookup_lookup"
   };

   const apexInvoker = lds.getApexInvoker("", "lookup", "searchDB", false);
   wireService.register(apexInvoker, lds.generateGetApexWireAdapter("", "lookup", "searchDB", false));

   class LookupLWC extends lwc.LightningElement {
     constructor(...args) {
       super(...args);
       this.selItem = void 0;
       this.lookupIcon = 'standard:contact';
       this.objectName = void 0;
       this.fieldApiText = void 0;
       this.fieldApiVal = void 0;
       this.fieldApiSearch = void 0;
       this.limit = void 0;
       this.placeholder = void 0;
       this.server_result = void 0;
       this.last_SearchText = void 0;
       this.last_ServerResult = void 0;
     }

     get selItemIsEmpty() {
       return this.selItem ? false : true;
     }

     serverCall(event) {
       let target = event.target;
       let searchText = target.value;
       let last_SearchText = this.last_SearchText; //Escape button pressed 

       if (event.keyCode === 27 || !searchText.trim()) {
         this.clearSelection();
       } else if (searchText.trim() !== last_SearchText && searchText.trim().length >= 2) {
         //&& (/\s+$/.test(searchText) || /-+$/.test(searchText) || /[0-9]+$/.test(searchText))){ 
         //Save server call, if last text not changed
         //Search only when space character entered            
         console.log('objName: ' + this.objectName);
         console.log('fieldApiText: ' + this.fieldApiText);
         console.log('fieldApiVal: ' + this.fieldApiVal);
         console.log('limit: ' + this.limit);
         console.log('fieldApiSearch: ' + this.fieldApiSearch);
         console.log('searchText: ' + searchText);
         console.log('-----------------------');
         apexInvoker({
           objectName: this.objectName,
           fld_API_Text: this.fieldApiText,
           fld_API_Val: this.fieldApiVal,
           lim: this.limit,
           fld_API_Search: this.fieldApiSearch,
           searchText: searchText
         }).then(result => {
           let retObj = JSON.parse(result);
           console.log(JSON.stringify(retObj));

           if (retObj.length <= 0) {
             let noResult = JSON.parse('[{"text":"No Results Found"}]');
             this.server_result = noResult;
             this.last_ServerResult = noResult;
           } else {
             this.server_result = retObj;
             this.last_ServerResult = retObj;
           }

           this.last_SearchText = searchText.trim();
         }).catch(error => {
           console.log(error);
         });
       } else if (searchText && last_SearchText && searchText.trim() === last_SearchText.trim()) {
         this.server_result = this.last_ServerResult;
         console.log('Server call saved');
       }
     }

     clearSelection() {
       this.selItem = null;
       this.server_result = null;
     }

     get resultNotNull() {
       if (this.server_result) return true;
       return false;
     }

   }

   lwc.registerDecorators(LookupLWC, {
     publicProps: {
       selItem: {
         config: 0
       },
       lookupIcon: {
         config: 0
       },
       objectName: {
         config: 0
       },
       fieldApiText: {
         config: 0
       },
       fieldApiVal: {
         config: 0
       },
       fieldApiSearch: {
         config: 0
       },
       limit: {
         config: 0
       },
       placeholder: {
         config: 0
       }
     },
     track: {
       server_result: 1
     }
   });

   var _cLookup = lwc.registerComponent(LookupLWC, {
     tmpl: _tmpl$3
   });

   function tmpl$3($api, $cmp, $slotset, $ctx) {
     const {
       c: api_custom_element
     } = $api;
     return [api_custom_element("c-lookup", _cLookup, {
       props: {
         "objectName": "Account",
         "fieldApiText": "Name",
         "fieldApiVal": "Id",
         "limit": "5",
         "fieldApiSearch": "Name",
         "lookupIcon": "standard:account",
         "selItem": $cmp.selItem,
         "placeholder": "Search Locations..."
       },
       key: 2
     }, [])];
   }

   var _tmpl$4 = lwc.registerTemplate(tmpl$3);
   tmpl$3.stylesheets = [];
   tmpl$3.stylesheetTokens = {
     hostAttribute: "lwc-lookupTestContainer_lookupTestContainer-host",
     shadowAttribute: "lwc-lookupTestContainer_lookupTestContainer"
   };

   class LookupTestContainer extends lwc.LightningElement {
     constructor(...args) {
       super(...args);
       this.selItem = void 0;
     }

   }

   lwc.registerDecorators(LookupTestContainer, {
     track: {
       selItem: 1
     }
   });

   var lookupTestContainer = lwc.registerComponent(LookupTestContainer, {
     tmpl: _tmpl$4
   });

   return lookupTestContainer;

});
