public class XMLParser {
    public class QuoteItemWrapper{
    	public String productCode;
        public String quantity;
    }    
    
    @AuraEnabled(cacheable=true)
    public static String parseXML(String documentId){
        ContentVersion cv = [SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :documentId AND IsLatest = true LIMIT 1];
        String xml = cv.VersionData.toString();
            
        Dom.Document doc = new Dom.Document();
        doc.load(xml);        
        Dom.XMLNode markupSummary = doc.getRootElement();        
        
		List<Dom.XMLNode> totals = markupSummary.getChildElements();
        
        List<QuoteItemWrapper> wrapList = new List<QuoteItemWrapper>();
        
        for(Dom.XMLNode total : totals){
            QuoteItemWrapper quoteItem = new QuoteItemWrapper();
            quoteItem.productCode = total.getChildElement('Subject', null).getText().substring(0,6);
            
            if(total.getChildElement('Count', null) != null) quoteItem.quantity = total.getChildElement('Count', null).getText();
            else if(total.getChildElement('Length', null) != null) quoteItem.quantity = total.getChildElement('Length', null).getText();
            
            wrapList.add(quoteItem);
        }        
        return JSON.Serialize(wrapList);
    }
}