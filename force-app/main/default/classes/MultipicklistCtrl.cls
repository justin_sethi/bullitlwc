public class MultipicklistCtrl {
    /**
     * Returns JSON of list of picklist values to Lex Components
     * @objectName - Name of SObject
     * @fieldAPIName - API name of field to be searched
     * @recordTypeId - text to be searched
     * */
    @AuraEnabled(cacheable=true)     
    public static List<String> getPickvals(String objectName, String fieldName){
        //Get object layout from schema
        Schema.SObjectType t = Schema.getGlobalDescribe().get(objectName);         
        Schema.DescribeSObjectResult r = t.getDescribe();            
        Schema.DescribeFieldResult f;        
        
        //Get schema field description and set field type as a string
        f = r.fields.getMap().get(fieldName).getDescribe();                                        
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject 
        
        List<String> pickvalList = new List<String>();
        for(Integer i=0; i<pick_list_values.size(); i++){
            if(i!=pick_list_values.size()) pickvalList.add(pick_list_values[i].getValue());
        }
        return pickvalList;
    }
}