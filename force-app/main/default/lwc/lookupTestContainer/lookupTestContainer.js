/* eslint-disable no-console */
import { LightningElement, track } from "lwc";

export default class LookupTestContainer extends LightningElement {
  @track selectedAccount;
  @track selectedPickvals;

  handleSelectionChange(event) {
    this.selectedAccount = JSON.parse(event.detail);
    console.log("Parent component receiving event JSON");
    console.log(event.detail);
  }
}
