/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import searchDB from '@salesforce/apex/lookup.searchDB';

export default class LookupLWC extends LightningElement {
    @api lookupIcon = 'standard:contact'; //Lightning icon to use for lookup; default - contact
    @api objectName; //Name of Object to be searched
    @api fieldApiText; //API Name of field, used to show text
    @api fieldApiVal; //API Name of field, to be returned from component
    @api fieldApiSearch; //API Name of field to be searched
    @api limit; //Total number of records to be searched
    @api placeholder; //Placeholder when lookup is empty
    

    @track server_result;
    @track selItem; //This attribute holds the selected item
    last_SearchText;
    last_ServerResult;

    get selItemIsEmpty(){
        return (this.selItem) ? false : true;
    }

    serverCall(event){
        let target = event.target;  
        let searchText = target.value; 
        let last_SearchText = this.last_SearchText;
        //Escape button pressed 
        if (event.keyCode === 27 || !searchText.trim()) { 
            this.clearSelection();
        } else if(searchText.trim() !== last_SearchText && searchText.trim().length >= 2){ //&& (/\s+$/.test(searchText) || /-+$/.test(searchText) || /[0-9]+$/.test(searchText))){ 
            //Save server call, if last text not changed
            //Search only when space character entered            
            searchDB({ 
                objectName : this.objectName,
                fld_API_Text : this.fieldApiText,
                fld_API_Val : this.fieldApiVal,
                lim : this.limit,
                fld_API_Search : this.fieldApiSearch,
                searchText : searchText
            })
            .then(result => {
                let retObj = JSON.parse(result);         
                if(retObj.length <= 0){
                    let noResult = JSON.parse('[{"text":"No Results Found"}]');
                    this.server_result = noResult;
                    this.last_ServerResult = noResult;
                }else{
                    this.server_result = retObj; 
                    this.last_ServerResult = retObj;
                }  
                this.last_SearchText = searchText.trim();
            })
            .catch(error => {
                console.log(error);
            });
                    
        } else if(searchText && last_SearchText && searchText.trim() === last_SearchText.trim()){ 
            this.server_result = this.last_ServerResult;
            console.log('Server call saved');
        }      
    }

    clearSelection(){
        this.selItem=null;
        this.server_result=null;

        const clearedEvent = new CustomEvent("selectionchange", {
            detail: null
        });          
        this.dispatchEvent(clearedEvent);
    }

    get resultNotNull(){
        if(this.server_result) return true;
        return false;
    }

    itemSelected(e){        
        let itemval = e.currentTarget.dataset.itemval;
        let item;
        for(let result of this.server_result){
            if(itemval === result.val){
                item = result;
                break;                
            }
        }

        if(item && item.val){
            this.selItem = item;
            this.last_ServerResult = this.server_result;
            this.server_result = null;

            const selectedEvent = new CustomEvent("selectionchange", {                
                detail: JSON.stringify(this.selItem)
            });          
            this.dispatchEvent(selectedEvent);
        }else{
            const nullEvent = new CustomEvent("selectionchange", {
                detail: null
            });          
            this.dispatchEvent(nullEvent);
        }
    }
}