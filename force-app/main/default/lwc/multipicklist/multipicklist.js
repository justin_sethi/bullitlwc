/* eslint-disable no-console */
import { LightningElement, api, track } from "lwc";
import getPickvals from "@salesforce/apex/MultipicklistCtrl.getPickvals";

export default class Multipicklist extends LightningElement {
  @api objectName; //API name of object that contains pickval.
  @api fieldApiName; //API name of multi-select picklist field.
  @api placeholder = "Select items..."; //Placeholder text that is displayed in the picklist field.
  @api limitHeight = false; //Forces component to scroll if the height gets too large.
  @api maxHeight = 50; //Max height in pixels. Limit height must be true for this to take effect.

  @track selectedItems = []; //This attribute holds the selected pickvals.
  @track allPickvals = []; //All the possible pickvals for the field.
  @track remainingPickvals = []; //The pickvals that have not been selected.
  @track expandPicklist = false; //Determines whether or not the dropdown is expanded.
  initialRender = false;

  //Initialization method. Gets possible pickvals from server.
  connectedCallback() {
    getPickvals({
      objectName: this.objectName,
      fieldName: this.fieldApiName
    })
      .then(result => {
        if (result && result.length !== 0) {
          this.allPickvals = result;
          this.remainingPickvals = result;
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  renderedCallback() {
    if (this.limitHeight && !this.initialRender) {
      let selectionContainer = this.template.querySelector(".selectionContainer");
      if (selectionContainer) {
        const maxHeight = this.maxHeight
        selectionContainer.style.overflowY = "auto";
        selectionContainer.style.maxHeight = maxHeight + "px";
        selectionContainer.style.paddingRight = '5px';
        this.initialRender = true;
      }
    }
  }

  get selectedItemsEmpty() {
    return this.selectedItems && this.selectedItems.length !== 0 ? false : true;
  }

  get remainingPickvalsEmpty() {
    return this.remainingPickvals && this.remainingPickvals.length !== 0
      ? false
      : true;
  }

  get remainingPickvalsAndExpanded() {
    return (
      this.remainingPickvals &&
      this.remainingPickvals.length !== 0 &&
      this.expandPicklist
    );
  }

  //Changes whether dropdown is expanded
  toggleSelection() {
    this.expandPicklist = this.expandPicklist ? false : true;
  }

  //Removes all selections
  clearSelections() {
    this.selectedItems = [];
    this.remainingPickvals = this.allPickvals;

    this.fireSelectionChangeEvent();
  }

  //Fires event with selected items for parent to read
  fireSelectionChangeEvent() {
    const selectionEvent = new CustomEvent("selectionchange", {
      detail: JSON.stringify(this.selectedItems)
    });
    this.dispatchEvent(selectionEvent);
  }

  //Moves item to selection list and removes from remaining items
  itemSelected(event) {
    const selectedItem = event.target.dataset.itemname;

    let selectedItems = this.selectedItems;
    selectedItems.push(selectedItem);
    this.selectedItems = selectedItems;

    let pickvals = this.allPickvals;
    let remainingPickvals = this.remainingPickvals;
    let newPickvalList = [];

    for (let pickval of pickvals) {
      if (remainingPickvals.includes(pickval) && pickval !== selectedItem)
        newPickvalList.push(pickval);
    }
    if (newPickvalList.length === 0) this.expandPicklist = false;
    this.remainingPickvals = newPickvalList;

    this.fireSelectionChangeEvent();
  }

  //Removes item from selection list and adds to remaining items
  removeSelection(event) {
    const selectedItem = event.target.dataset.itemname;

    let newSelectedItems = [];
    const selectedItems = this.selectedItems;

    for (let item of selectedItems) {
      if (item !== selectedItem) newSelectedItems.push(item);
    }
    this.selectedItems = newSelectedItems;

    let pickvals = this.allPickvals;
    let remainingPickvals = this.remainingPickvals;
    let newPickvalList = [];

    for (let pickval of pickvals) {
      if (remainingPickvals.includes(pickval) || pickval === selectedItem)
        newPickvalList.push(pickval);
    }
    this.remainingPickvals = newPickvalList;

    this.fireSelectionChangeEvent();
  }
}
