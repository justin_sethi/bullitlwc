import { LightningElement, track } from "lwc";

export default class MultipicklistTestContainer extends LightningElement {
  @track selectedPickvals = [];

  get selectedItemsNotEmpty() {
    return this.selectedPickvals && this.selectedPickvals.length !== 0
      ? true
      : false;
  }

  handleMultipicklistChange(event) {
    this.selectedPickvals = JSON.parse(event.detail);
  }
}
