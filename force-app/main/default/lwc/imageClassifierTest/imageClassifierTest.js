/* eslint-disable no-console */
import { LightningElement, track } from 'lwc';
import getPredictionFromContent from '@salesforce/apex/VisionCtrl.getPredictionFromContent';

export default class ImageClassifierTest extends LightningElement {
    recordId='0016g000004bCQTAA2';
    @track prediction;

    get predictionExists(){
        if(this.prediction != null) return true;
        return false;        
    }

    handleUploadFinished(event){
        const uploadedFiles = event.detail.files;
        let docId = uploadedFiles[0].documentId;
        getPredictionFromContent({ 
            documentId : docId,
            modelId : 'VAVKPWIOOY5FCEZCX5NEM5VN6I'            
        })
        .then(result => {
            if(result != null){
                let response = JSON.parse(result);
                if(response != null){
                    let bestPrediction;
                    
                    for(let prediction of response){
                        if(!bestPrediction) bestPrediction = prediction;
                        else if(prediction.probability > bestPrediction.probability) bestPrediction = prediction;
                    }
                    let predictionPercentage = (bestPrediction.probability*100);
                    if(bestPrediction.label  === 'Laptop') this.prediction = 'This is a: ' + bestPrediction.label + ' (' + predictionPercentage + '%)'; 
                    else if(bestPrediction.label  === 'Sanitizer') this.prediction = 'This is: Hand ' + bestPrediction.label + ' (' + predictionPercentage + '%)'; 
                    else this.prediction = 'This is: ' + bestPrediction.label + ' (' + predictionPercentage + '%)';     
                }     
            }
        })
        .catch(error => {
            console.log(error);
        });
    }
}