/* eslint-disable no-console */
import { LightningElement, track, api } from 'lwc';
import parseXML from '@salesforce/apex/XMLParser.parseXML';

export default class XmlFileParser extends LightningElement {
    @api recordId
    @track parseList;

    get parseListNotEmpty(){
        if(this.parseList != null) return true;
        return false;        
    }

    handleUploadFinished(event){
        const uploadedFiles = event.detail.files;
        let docId = uploadedFiles[0].documentId;
        parseXML({ 
            documentId : docId
        })
        .then(result => {
            if(result != null){
                let retObj = JSON.parse(result);                 
                this.parseList = retObj;
            }
        })
        .catch(error => {
            console.log(error);
        });
    }
}